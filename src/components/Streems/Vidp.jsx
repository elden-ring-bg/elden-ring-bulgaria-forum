import './Streams.css'
const Vidp = ({url,streamer}) => {
  console.log(url)
  return (<div id="vidp-box">
    <h1 id='streamer'>{streamer}</h1>
    <iframe src={url} id="stream" muted="true" frameborder="0" allowfullscreen="true" scrolling="no" height="178" width="220"></iframe>
    </div>
  )
}
export default Vidp