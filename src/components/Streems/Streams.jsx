//secret = lauftysnpyx8vj9a34ce9pdrt5ykf8
//id = fsct220f8ym2s51o66mfr3ngwnkgrw
import './Streams.css'
import Main from '../Main/Main'
import Vidp from './Vidp'

const Streems = () => {
  const streamUrls2 = {
    'the_happy_hob':'https://player.twitch.tv/?channel=the_happy_hob&parent=localhost',
    'elxokas':'https://player.twitch.tv/?channel=elxokas&parent=localhost',
    'woolieversus':'https://player.twitch.tv/?channel=woolieversus&parent=localhost',
    'scottjund':'https://player.twitch.tv/?channel=scottjund&parent=localhost'
  }
  return (
    <Main>
      <div id='streams-container'>
        {Object.keys(streamUrls2).map((name,key) => (
          <Vidp url={streamUrls2[name]} streamer={name}></Vidp>
        ))}
      </div>
    </Main>
  )
}
export default Streems