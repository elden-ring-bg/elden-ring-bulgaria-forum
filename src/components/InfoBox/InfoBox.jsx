
import { Link,useNavigate ,NavLink} from 'react-router-dom'
import './InfoBox.css'
const InfoBox = (data) => {
  return (<div>
    <Link to={'/'+data.route}>
      <div className='info-box-container'>
        {data.num === 0 ?  <h1>{data.labeled}</h1> : <></>}
        <div id="glow">
        <p>" {data.text} "</p>
        <p id="info-box-category">{data.category}</p>
        </div>
      </div>
    </Link>
  </div>
  )
}
export default InfoBox