import React from 'react';
import Main from '../Main/Main';
import './Media.css';

export const Media = () => {
  return (
    <Main title='Media'>
      <div className='gallery'>
        <a target="_blank" href='https://asset.vg247.com/elden-ring-beginners-guide.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-beginners-guide.jpg'>
          <img src='https://asset.vg247.com/elden-ring-beginners-guide.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-beginners-guide.jpg' alt='tarnished-on-torrent'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://cdn.mos.cms.futurecdn.net/JH4ftnfo6vTWomDc7E6GyC.jpg'>
          <img src='https://cdn.mos.cms.futurecdn.net/JH4ftnfo6vTWomDc7E6GyC.jpg' alt='godrick-the-grafted'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://assets2.rockpapershotgun.com/elden-ring-story-trailer.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-story-trailer.jpg'>
          <img src='https://assets2.rockpapershotgun.com/elden-ring-story-trailer.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-story-trailer.jpg' alt='waifu-ranni'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://cdn.mos.cms.futurecdn.net/mjq5tTpa4tPoBteYpv6urE.jpg'>
          <img src='https://cdn.mos.cms.futurecdn.net/mjq5tTpa4tPoBteYpv6urE.jpg' alt='celestial-view'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://cdn.mos.cms.futurecdn.net/fNyjvMw8evGHzbri3GWGRj.jpg'>
          <img src='https://cdn.mos.cms.futurecdn.net/fNyjvMw8evGHzbri3GWGRj.jpg' alt='malenia'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://www.acemanwolf.net/wp-content/uploads/2022/02/elden-ring-review.jpg'>
          <img src='https://www.acemanwolf.net/wp-content/uploads/2022/02/elden-ring-review.jpg' alt='castle'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://assets2.rockpapershotgun.com/elden-ring-sidequests-head.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-sidequests-head.jpg'>
          <img src='https://assets2.rockpapershotgun.com/elden-ring-sidequests-head.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-sidequests-head.jpg' alt='nature'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://twinfinite.net/wp-content/uploads/2022/02/Elden-Ring-can-you-pause.jpg'>
          <img src='https://twinfinite.net/wp-content/uploads/2022/02/Elden-Ring-can-you-pause.jpg' alt='elden-ring-game-cover'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://d2ofqe7l47306o.cloudfront.net/games/1366x768/elden-ring-22.jpg'>
          <img src='https://d2ofqe7l47306o.cloudfront.net/games/1366x768/elden-ring-22.jpg' alt='dragon-fight'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://gaminghybrid.com/wp-content/uploads/2022/03/1647235004_How-to-Beat-Maliketh-The-Black-Blade.jpg'>
          <img src='https://gaminghybrid.com/wp-content/uploads/2022/03/1647235004_How-to-Beat-Maliketh-The-Black-Blade.jpg' alt='malekith-the-black-blade'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://d2ofqe7l47306o.cloudfront.net/games/1366x768/elden-ring-23.jpg'>
          <img src='https://d2ofqe7l47306o.cloudfront.net/games/1366x768/elden-ring-23.jpg' alt='magic-giant'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://cdn.wccftech.com/wp-content/uploads/2021/12/godfrey_artworkHD-scaled.jpg'>
          <img src='https://cdn.wccftech.com/wp-content/uploads/2021/12/godfrey_artworkHD-scaled.jpg' alt='godfrey-artwork'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://d30xqvs6b65d10.cloudfront.net/wp-content/uploads/2022/04/elden-ring-how-to-beat-radagon-elden-beast-feat.jpg'>
          <img src='https://d30xqvs6b65d10.cloudfront.net/wp-content/uploads/2022/04/elden-ring-how-to-beat-radagon-elden-beast-feat.jpg' alt='radagon'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://asset.vg247.com/elden_ring_rennala_guide_boss_scene_10.jpg/BROK/thumbnail/1600x900/format/jpg/quality/80/elden_ring_rennala_guide_boss_scene_10.jpg'>
          <img src='https://asset.vg247.com/elden_ring_rennala_guide_boss_scene_10.jpg/BROK/thumbnail/1600x900/format/jpg/quality/80/elden_ring_rennala_guide_boss_scene_10.jpg' alt='rennala'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://assets2.rockpapershotgun.com/elden-ring-dung-eater-cinematic.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-dung-eater-cinematic.jpg'>
          <img src='https://assets2.rockpapershotgun.com/elden-ring-dung-eater-cinematic.jpg/BROK/resize/1920x1920%3E/format/jpg/quality/80/elden-ring-dung-eater-cinematic.jpg' alt='dung-eater'/>
        </a>
      </div>
      <div className='gallery'>
        <a target='_blank' href='https://wallpaperaccess.com/full/2651910.jpg'>
          <img src='https://wallpaperaccess.com/full/2651910.jpg' alt='godskin'/>
        </a>
      </div>
    </Main>
  );
};
