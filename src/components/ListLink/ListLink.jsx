import './ListLink.css'
import { Link } from 'react-router-dom'
import pin from '../../assets/pin2.png'
const ListLinks = (data) => {
  return (
  <div>
    <Link to={data.route}>
      <div className='list-link-container'>
      <img className='square-img' src={pin}></img>
     <p>{data.text}</p>
    </div></Link>
  </div>
  )
}
export default ListLinks