import React from 'react';
import Main from '../Main/Main';
import './Map.css';

export const Map = () => {
  return (
    <Main title='Interactive Map'>
      <iframe title='elden-ring-interactive-map' src='https://mapgenie.io/elden-ring/maps/the-lands-between?x=-0.5398282136806642&y=0.7200606474521294&zoom=10.935346244077452' className='interactive-map'></iframe>
    </Main>
  );
};
