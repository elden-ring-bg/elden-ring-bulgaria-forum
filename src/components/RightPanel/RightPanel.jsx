import { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import { AppContext } from '../../providers/AppContext'
import { logoutUser } from '../../services/auth.service'
import BoxLink from '../BoxLink/BoxLink'
import ListLink from '../ListLink/ListLink'
import './RightPanel.css'
import zasaita from '../../assets/zasaita.png'
import forum from '../../assets/forum.png'
import media from '../../assets/media.png'
const RightPanel = () => {

  const { user, userData, setContext } = useContext(AppContext);
  const logout = () => {
    logoutUser()
    .then(() => {
      setContext({ user: null, userData: null });
    })
  }

  return (user ?
    <div className="right-panel">
      <div id='spacer'></div>
      <div className='profile-container'>
        <p>Welcome, <NavLink to={'profile'}>{userData.username}</NavLink></p>
        <NavLink className={'logout-button'} to={'/home'} onClick={logout} >Изход</NavLink>
      </div>
      <BoxLink text={'Форум'} route={'forum'} source={forum}></BoxLink>
      <BoxLink text={'Медия'} route={'media'} source={media}></BoxLink>
      <ListLink text={'Видеа'} route={'videos'}> </ListLink>
      <ListLink text={'Галерия'} route={'gallery'}></ListLink>
      <ListLink text={'Стриймове'} route={'streams'}></ListLink>
      <BoxLink text={'За Сайта'} route={'about'} source={zasaita}></BoxLink>
    </div>
  : 
    <div className="right-panel">
      <div className='profile-container'>
        <NavLink to='/register'><p>Регистрация</p></NavLink>
        <NavLink to='/login'><p>Вход</p></NavLink>
      </div>
      <BoxLink text={'Форум'} route={'forum'} source={forum}></BoxLink>
      <BoxLink text={'Медия'} route={'media'} source={media}></BoxLink>
      <ListLink text={'Видеа'} route={'videos'}> </ListLink>
      <ListLink text={'Галерия'} route={'gallery'}></ListLink>
      <ListLink text={'Стриймове'} route={'streams'}></ListLink>
      <BoxLink text={'За Сайта'} route={'about'} source={zasaita}></BoxLink>
    </div>)
}
export default RightPanel