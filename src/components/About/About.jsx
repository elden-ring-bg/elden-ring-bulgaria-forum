import Main from "../Main/Main"
import './About.css'
const About = () => {
  return (
    <Main title="About">
      <div id="about-container">
      <h1>За Сайта</h1>
      <p>Използвани технологии</p>
      <p>Frondend - <a href="https://reactjs.org/" target="_blank">React</a></p>
      <p>Database/Storage/Hosting - <a href="https://firebase.google.com" target="_blank">Firebase</a></p>
      <p>Карта на играта - <a href="https://mapgenie.io/" target="_blank">Map Genie</a></p>
      <p>Пост едитор - <a href="https://ckeditor.com/" target="_blank">CKEditor</a></p>
      <div id="creators-container">
        <h1>Програмисти</h1>
        <div id='creator'>
          <h2>Илия Стоилов</h2>
          <p>irstoilov@gmail.com</p>
          <p><a href="https://gitlab.com/irstoilov" target="_blank">GitLab</a></p>
        </div>
        <div id='creator'>
          <h2>Симеон Симеонов</h2>
          <p>SimeonSimeonov@gmail.com</p>
          <p><a href="https://gitlab.com/ssimeonov32" target="_blank">GitLab</a></p>
        </div>
      </div>
      </div>
    </Main>
  )
}
export default About