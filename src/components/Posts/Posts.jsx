import { useNavigate ,useParams,useLocation} from "react-router-dom"
import { useContext,useEffect, useState } from "react"
import { createPostToTopic , getAllPostsForTopic, updatePost, deleteOnlyPostTopic, addLikedPosts, getAllAuthors, getTopic} from "../../services/category.service"
import { AppContext } from '../../providers/AppContext'
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import './Posts.css'
import Editor from "../Editor/Editor"
import Main from "../Main/Main";
import goodPng from '../../assets/Good2.png'
import poorPng from '../../assets/Poor.png'
import msg from '../../assets/elden-ring-messages2.png'
import Loading from "../Loading/Loading";
import Swal from 'sweetalert2';
import { storage } from "../../config/firebase-config";
import { v4 as uuidv4 } from 'uuid';
import { ref as storageRef, getDownloadURL, uploadBytes } from 'firebase/storage';
import { titles } from "../../providers/titles";
import { updateProfileTitle } from "../../services/users.service";
const Posts = () => {
  const { user,userData ,setContext} = useContext(AppContext)
  const navigate = useNavigate()
  const {id} = useParams()
  const [form, setForm] = useState({description: ''})
  const [content,setContent] = useState('')
  const [postImages,setPostImages] = useState([])
  const [posts,setPosts] = useState([])
  const [dummy,setDummy] = useState(0)
  const [authors,setAuthors] = useState([])
  const [editor,setEditor] = useState([])
  const [topic,setTopic] = useState('')

  const update = (prop) => (event) => {
    setForm({
      ...form,
      [prop]: event.target.value
    })
  }
  const updateUserDataContext = (postId,operation = null) => {
    if(operation) {
      userData.posts = userData.posts.filter((el) => el !== postId)  
    } else {
      if(userData.posts[0] == ['']) {
        userData.posts[0] = postId
      } else {
        userData.posts.push(postId)
      }
    }
    const userPosts = userData.posts.length
    if(userPosts >= 5 && userPosts < 10) {
      userData.title = titles['apprentice']
      updateProfileTitle(userData.username, titles['apprentice'])
    } else if(userPosts >= 10 && userPosts < 25) {
      updateProfileTitle(userData.username, titles['intermediate'])
      userData.title = titles['intermediate']
    } else if(userPosts >= 25 && userPosts < 50) {
      updateProfileTitle(userData.username, titles['advanced'])
      userData.title = titles['advanced']
    } else if(userPosts >= 50 && userPosts < 100) {
      updateProfileTitle(userData.username, titles['expert'])
      userData.title = titles['expert']
    } else if(userPosts >= 100 && userPosts < 150) {
      updateProfileTitle(userData.username, titles['master'])
      userData.title = titles['master']
    } else if(userPosts >= 150) {
      updateProfileTitle(userData.username, titles['god'])
      userData.title = titles['god']
    } else {
      updateProfileTitle(userData.username, titles['novice'])
      userData.title = titles['novice']
    }
    setContext({
      user,userData: {
        ...userData
      }
    })
  }

  const updateData = async () => {
    const data = await getAllPostsForTopic(id)
    const authorData = await getAllAuthors(data)
    const res = await getTopic(id)
    setTopic(res)
    setAuthors(authorData)
    setPosts(data)
  }
  useEffect(() => {
    updateData()
  },[])
  const createPost = async (e) => {
    e.preventDefault()
    editor.setData('')
    if(!userData) {
      setTimeout(() => {
        navigate('/login')
      },1500)
      return (Swal.fire({
        icon: 'error',
        title: 'Hey, maidenless...!',
        text: 'You must first Login',
        color: 'white',
        background: 'black',
      }));
    }
    form.description = content
    if(!form.description) {
      return alert('Please add a post')
    }
    const postId = await createPostToTopic(id,new Date(),form.description,userData.username,postImages)
    let updatedPosts = posts
    if(updatedPosts[0] === "") {
      updatedPosts[0] = postId[1]
    } else {
      updatedPosts.push(postId[1])
    }
    updateUserDataContext(postId[0])
    setPosts(updatedPosts)
    let authorsList = authors
    if(!authorsList[0]) {
      authorsList[0] = userData
    }else {
      authorsList.push(userData)
    }
    setAuthors(authorsList)
  }
  const renderHtml = (post) => {
    return {__html:`${post}`}
  }
  const handleLike = async (post,key) => {
    if(!userData) {
      setTimeout(() => {
        navigate('/login')
      },1500)
      return (Swal.fire({
        icon: 'error',
        title: 'Hey, maidenless...!',
        text: 'You must first Login',
        color: 'white',
        background: 'black',
      }));
    }
    if(post.likedBy.includes(userData.username)) {
      post.likedBy = post.likedBy.filter((ids) => ids !== userData.username)
      if(post.likedBy.length == 0) {
        post.likedBy = ['']
      }
      console.log(post.likedBy)
      post.likes--
      let updatedPosts = posts
      updatedPosts[key] = post
      setPosts(updatedPosts)
      await addLikedPosts(userData.username,post.id)
      await updatePost(post.id,post)
      setDummy(Math.random()*100)
    } else {
      if(post.likedBy[0] === '') {
        post.likedBy[0] = userData.username
      } else {
        post.likedBy.push(userData.username)
      }
      post.likes++
      let updatedPosts = posts
      updatedPosts[key] = post
      setPosts(updatedPosts)
      await addLikedPosts(userData.username,post.id)
      await updatePost(post.id,post)
      setDummy(Math.random()*100)
    }
  }

  const handleDelete = async(post,key) => {
    await deleteOnlyPostTopic(post,post.topic)
    let updatedPosts = posts
    updatedPosts = updatedPosts.filter((el) => el.id !== post.id)
    if(updatedPosts.length === 0 ) {
      updatedPosts = [""]
    }
    let authorsList = authors
    authorsList.splice(authorsList.indexOf(post.author),1)
    setAuthors(authorsList)
    setPosts(updatedPosts)
    setPostImages([])
    updateUserDataContext(post.id,"delete")
  }
  const fillAuthors = async() => {
    await getAllAuthors(posts)
  }
  
  class MyUploadAdapter {
    constructor(props) {
      this.loader = props;
    }
    upload() {
      return new Promise((resolve,reject) => {
        this.uploadToFireBase(resolve,reject)
      })
    }
    uploadToFireBase(resolve,reject) {
      this.loader.file
      .then(file => {
        let picKey = uuidv4()
        const pic = storageRef(storage, `/postImages/${picKey}`);
        let postImg = [...postImages]
        postImg.push(picKey)
        setPostImages(postImg)
        uploadBytes(pic, file)
          .then((snap) => {
            return getDownloadURL(snap.ref)
          })
          .then((url) => {
            this.loader.uploaded = url
            resolve({ default:url})
        })
      })
    }
  }
  if(posts.length == 0) {
    return ( <Main title="Posts"><div id="loading-posts-div"><Loading /></div></Main>)
  }
  function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get( 'FileRepository' ).createUploadAdapter = (loader) => {
      return new MyUploadAdapter(loader)
    }
  }
  const editorConfig = {
    extraPlugins:[MyCustomUploadAdapterPlugin],
    toolbar: {
      items:[
        'heading',
        '|',
        'bold',
        'italic',
        'link',
        'bulletedList',
        'numberedList',
        '|',
        'imageUpload',
        'undo',
        'redo'
      ]
    }
  }

  const goBack = () => {
    navigate(`/category/${topic.category}`)
  } 
  return(
    <Main title="Posts">
    <div>
      {posts[0] == "" ? 
        <div>
          <div id='go-back'><h1 onClick={goBack}>Обратно към Теми</h1></div>
          <h1>Първият пост е ваш</h1>
        </div> 
        : <div className="post-container">
          <div id='go-back'><h1 onClick={goBack}>Обратно към Теми</h1></div>
        {posts.map((post,key) => (
          <div className="post-text">
            <div className="poster-box">
              <h1 id="post-username">{post.author}</h1>
              <img id="post-avatar-img" src={authors[key].profilePicUrl}></img>
              <p>{authors[key].title}</p>
              <div className="post-msg-box">
                <img src={msg} className="post-msg-img"></img>
                <p className="post-msg-p">{authors[key].posts.length}</p>
              </div>
            </div>
            <div className="post-content">
              <div className="post-heading">
              <h1 id="post-title">{post.title}</h1>
              <p>Автор: <span id="post-heading-author">{post.author}</span> на <span>{new Date(post.createdOn).toLocaleDateString()}</span> - <span>{new Date(post.createdOn).toLocaleTimeString('it-IT')}</span><span id="likes">{post.likes}</span><img src={post.likedBy?.includes(userData?.username) ? poorPng : goodPng} id={post.likedBy?.includes(userData?.username) ? 'poor' : 'good'} onClick={(e) => handleLike(post,key)}></img></p>
              <p id="breakline"></p>
              </div>
              <div dangerouslySetInnerHTML={renderHtml(post.description)} id="danger-text"/>
              <span id={post.author == userData?.username ? "delete-post" :  userData?.role == 3 ? 'delete-post' : 'not-delete-post'} onClick={(e) => handleDelete(post,key)}>Изтрий</span>
            </div>
          </div>
        ))}
      </div>
      }
      <div>
        <form onSubmit={createPost}>
          <div className='editor-container'>
            <CKEditor
              editor={ ClassicEditor }
              config={editorConfig}
              data=""
              onReady={ editor => {
                setEditor(editor)
            }}
              onChange={ ( event, editor ) => {
              const data = editor.getData();
              setContent(data)
            }}
            onBlur={ ( event, editor ) => {
            }}
            onFocus={ ( event, editor ) => {
            }}
            />
          </div>
          <button type="submit" onSubmit={createPost}>Създай Пост</button>
        </form>
      </div>
    </div>
  </Main>
  )
}
export default Posts