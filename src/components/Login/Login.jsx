import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AppContext } from '../../providers/AppContext';
import { loginUser } from '../../services/auth.service';
import { getUserData } from '../../services/users.service';
import Main from '../Main/Main';
import Swal from 'sweetalert2';
import './Login.css';

export const Login = () => {

  const [form, setForm] = useState({ email: '', password: '' });
  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();

  const update = (prop) => (event) => {
    setForm({
      ...form,
      [prop]: event.target.value
    });
  };

  const login = (e) => {
    e.preventDefault();
    // TODO: Display proper errors when trying to login with incorrect credentials

    loginUser(form.email, form.password)
    .then((user) => {
      return getUserData(user.user.uid)
      .then((snap) => {
        if (snap.exists()) {
          setContext({
            user: user.user,
            userData: snap.val()[Object.keys(snap.val())[0]],
          });
          Swal.fire({
            icon: 'success',
            title: 'Hey there tarnished!',
            text: 'Welcome to Elden Ring BG.',
            color: 'white',
            background: 'black',
          })
          navigate('/profile');
        }
      }).catch(Swal.fire({
        icon: 'error',
        title: 'Hey there tarnished!',
        text: 'The email or password you entered is invalid.',
        color: 'white',
        background: 'black',
      }));
    })
  }
  
  return (
    <Main title="Login">
      <form className='Login'>
        <p className='text-for-dummies'>Въведете вашия и-мейл адрес</p>
        <input className="login-input" autoComplete="off" placeholder="E-Mail" type="email" id="email" value={form.email} onChange={update('email')}></input>
        <p className='text-for-dummies'>Въведете вашата парола</p>
        <input className="login-input" autoComplete="off" placeholder="Password" type="password" id="password" value={form.password} onChange={update('password')}></input>
        <button onClick={login} className="login-button">Влез в акаунта си</button>
      </form>
    </Main>
  );
}
