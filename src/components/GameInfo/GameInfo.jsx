import React from 'react';
import { NavLink } from 'react-router-dom';
import Main from '../Main/Main';
import './GameInfo.css';

export const GameInfo = () => {
  return (
    <Main title='Game Info'>
      <div className='game-info-section'>
        <h3>Системни изисквания</h3>
        <div className='minimum'>
          <h4><u>Минимални</u></h4>
          <p className='system-req'>Операционна система: Windows 10</p>
          <p className='system-req'>Процесор: Intel Core i5-8400 или AMD Ryzen 3 3300x</p>
          <p className='system-req'>Памет: 12GB RAM</p>
          <p className='system-req'>Графичен процесор: NVIDIA GeForce GTX 1060, 3GB или AMD Radeon RX 580, 4GB</p>
        </div>
        <div className='recommended'>
          <h4><u>Препоръчителни</u></h4>
          <p className='system-req'>Операционна система: Windows 10/11</p>
          <p className='system-req'>Процесор: Intel Core i7-8700k или AMD Ryzen 5 3600x</p>
          <p className='system-req'>Памет: 16GB RAM</p>
          <p className='system-req'>Графичен процесор: NVIDIA GeForce GTX 1070, 8GB или AMD Radeon RX Vega 56, 8GB</p>
        </div>
        <div>
          <NavLink to={'/map'} className={'interactive-nav'}>Линк към интерактивна карта на играта</NavLink>
        </div>
      </div>
    </Main>
  );
};
