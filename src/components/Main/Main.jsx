import { useEffect } from 'react';
import { Container } from '../Container/Container';
import './Main.css';

const Main = (props) => {

  useEffect(() => {
    document.title = `${props.title} | Elden Ring Bulgaria`
    window.scrollTo(0, 0)
  }, []);

  return (<div id='MAINED'>
      <Container>{ props.children }</Container>
    </div>
  )
}
export default Main;