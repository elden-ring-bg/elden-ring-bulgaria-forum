import { useContext } from 'react';
import { AppContext } from '../../providers/AppContext';
import { ref as storageRef, getDownloadURL, uploadBytes } from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import Loading from '../Loading/Loading';
import Main from '../Main/Main';
import './Profile.css';
import { updateProfilePicture, updateProfileSteamId } from '../../services/users.service';
import { v4 as uuidv4 } from 'uuid';

const Profile = () => {

  const { user, userData, setContext } = useContext(AppContext);

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if(!file) {
      return;
    }

    const pic = storageRef(storage, `/avatars/${uuidv4()}`);

    uploadBytes(pic, file)
    .then((snap) => getDownloadURL(snap.ref)
    .then((url) => {
      return updateProfilePicture(userData.username, url)
      .then(() => {
        setContext({
          user, userData: {
            ...userData,
            profilePicUrl: url,
          }
        })
      })
    })).catch(console.error);
  };

  const updateSteamId = (e) => {
    e.preventDefault();

    const input = e.target[0].value;

    if (!input) {
      return;
    }

    updateProfileSteamId(userData.username, input)
    .then(() => {
      setContext({
        user, userData: {
          ...userData,
          steamId: input,
        }
      })
    }).catch((e) => e.message);
  }
  
  return (user ?
    <Main title='Profile'>
      <div className="Profile">
        <div className='left-side'>
          <img src={`${userData.profilePicUrl}`} className='avatar' alt='Avatar'/>
          <h3>Постове: {userData.posts.length ? userData.posts.length : 0}</h3>
          <h3>Титла: {userData.title}</h3>
          <form onSubmit={uploadPicture} className="update-form">
          <label className='avatar-label'>Избери снимка
            <input id="pic" type="file" name="file" className='file-choosing-button' accept="image/png, image/jpg, image/gif, image/jpeg"></input>
          </label><br/>
          <button type="submit" className='profile-button'>Качи</button>
        </form>
        </div>
        <div className='right-side'>
          <h3>Потребителско име: {userData.username}</h3>
          <h3>Имейл: {userData.email}</h3>
          <h3>Име: {userData.firstName}</h3>
          <h3>Фамилия: {userData.lastName}</h3>
          <form onSubmit={updateSteamId} className="update-form">
            <input type="text" name="steam-id" className='steam-id-input' placeholder='Steam ID' autoComplete="off"></input><br/>
            <button type="submit" className='profile-button'>Смени своето Steam ID</button>
          </form>
        </div>
      </div>
    </Main>
  :
    <Main title='Profile'>
      <Loading></Loading>
    </Main>
  );
};

export default Profile;