import './Footer.css';

const Footer = () => {

  return (
    <div className='Footer'>
      <span>Elden Ring® и From Software® са търговски марки или регистрирани търговски марки на Kadokawa Corporation.</span>
    </div>
  )
};

export default Footer;
