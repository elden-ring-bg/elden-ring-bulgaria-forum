import { Link , useNavigate } from 'react-router-dom'
import './CategoryBox.css'
import { deleteCategory, deleteEverythingCategory } from '../../services/category.service'
const CategoryBox = ({category ,updateData,role,pic}) => {
  const navigate = useNavigate()

  const handleClick = async () => {
    await deleteEverythingCategory(category.title)
    updateData()
  }
  return(
    <div className="category-box-container">
      <Link to={`/category/${category.title}`}><img className="category-img" src={category.img}></img></Link>
      <div className="text-holder">
        <Link to={ `/category/${category.title}`}><h1 className="category-title">{category.title}</h1></Link>
        <p className="category-description">{category.description}</p>
        {role === 3 ? <span className='delete-span' onClick={handleClick}>Delete</span> : <></>}
      </div>
    </div>
  )
}
export default CategoryBox