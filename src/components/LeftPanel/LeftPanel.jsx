import './LeftPanel.css'
import placeholderImage from '../../assets/vertical-placeholder-image.jpg'
import BoxLink from '../BoxLink/BoxLink'
import ListLinks from '../ListLink/ListLink'
import {getAllTopicsPosts} from '../../services/category.service'
import { useEffect, useState } from 'react'
import InfoBox from '../InfoBox/InfoBox'
import Loading from '../Loading/Loading'
import map from '../../assets/map.png'
const LeftPanel = () => {
  const [mostTopic,setMostTopic] = useState('')

  const getData = async () => {
    const data = await getAllTopicsPosts()
    setMostTopic(data)
  }
  useEffect(() => {
    getData()
  },[])
  if(mostTopic) {
    return (
      <div className="left-panel">
      <div id='spacer'></div>
      <BoxLink text={'Карта'} route={'map'} source={map}/>
      {mostTopic?.map((topic,key) => (
              <InfoBox text={topic.title} route={`posts${topic?.id}`} labeled={'Най-активни Tеми'} num={key} category={topic.category}/>
      ))}
    </div>
    )
  }
  return (
    <div className="left-panel">
      <div id='spacer'></div>
      <BoxLink text={'Карта'} route={'map'} source={map}/>
    </div>
  )
}
export default LeftPanel