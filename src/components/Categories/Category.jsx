import { useContext,useEffect, useState } from "react";
import Main from "../Main/Main";
import { AppContext } from '../../providers/AppContext'
import { createCategory, getCategories, getCategory } from "../../services/category.service";
import './Category.css'
import BoxLink from "../BoxLink/BoxLink";
import CategoryBox from "../CategoryBox/CategoryBox";
import Loading from "../Loading/Loading";
const Category = () => {
  const [form, setForm] = useState({ title: '', description: ''})
  const [categories,setCategories] = useState([])
  const { userData , setUser} = useContext(AppContext)
  const update = (prop) => (event) => {
    setForm({
      ...form,
      [prop]: event.target.value
    })
  }
  const updateData = async () => {
    const data = await getCategories()
    setCategories(data)
  }
  useEffect(() => {
    updateData()
  },[])

  const createCategory2 = async (e) => {  
    e.preventDefault();

    let data = await getCategory(form.title)
    if(data) {
      return alert('A Category with that name exists')
    }
    if (form.title == '' || form.description == '') {
      return alert('Please provide a Title and Description')
    }
    createCategory(form.title,form.description,[''])
    updateData()
  }
  if(categories.length == 0) {
    return ( <Main title="Categories"><Loading /></Main>)
  }
  return (
    <Main title="Categories">
      <div className='content-holder'>
        <div className="category-container">
          {categories.map((cat,key) => (
          <div className="cat">
          <CategoryBox category={cat} updateData={updateData} role={userData?.role} pic={userData?.profilePicUrl} key={key}/>
        </div>
        ))}
     </div>
     {userData?.role === 3 ?  <div className='category-form'>
       <div className="category-form-holder">
        <form onSubmit={createCategory2}>
          <div>
            <h1 id="category-form-title">Създай нова Категория</h1>
            <div>
              <label htmlFor='title' className="labels"></label>
              <input type="text" id="title" onChange={update('title')} value={form.title} className="registration-input" placeholder="Заглавие на Категорията"></input>
            </div>
            <div>
              <label htmlFor='handle' className="labels"></label>
              <input type="text" id="description" onChange={update('description')} value={form.description} className="registration-input" placeholder="Кратко описание"></input>
            </div>
            <button id="form-create"type="submit" onSubmit={createCategory2} className="register-button">Създай Категория</button>
          </div>
          </form>
        </div>
      </div>
       : <></>}
      </div>
    </Main>
 );
};

export default Category;