import { NavLink } from 'react-router-dom';
import './Header.css';

const Header = () => (
  <div className='Header'>
    <h1>Elden Ring Bulgaria</h1>
    <br></br>
    <NavLink to={'/home'} className={'navLi'} activeclassname={'active'}>Начало</NavLink>
    <NavLink to={'/gameinfo'} className={'navLi'} activeclassname={'active'}>За Играта</NavLink>
    <NavLink to={'/media'} className={'navLi'} activeclassname={'active'}>Медия</NavLink>
    <NavLink to={'/forum'} className={'navLi'} activeclassname={'active'}>Форум</NavLink>
  </div>)
export default Header;
