import { useState } from "react";
import Main from "../Main/Main";
import { getUserByUsername, createUserUsername } from "../../services/users.service";
import { registerUser } from "../../services/auth.service";
import './Register.css'
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

const Register = () => {

  const [form, setForm] = useState({ email: '', username: '', password: '', firstName: '', lastName: ''})
  const navigate = useNavigate();

  const update = (prop) => (event) => {
    setForm({
      ...form,
      [prop]: event.target.value
    })
  }

  const register = (e) => {
    
    e.preventDefault();

    if (form.username.length < 5 || form.username.length > 15) {
      return (Swal.fire({
        icon: 'error',
        title: 'Hey, maidenless...!',
        text: 'Username must be between 6 and 15 characters.',
        color: 'white',
        background: 'black',
      }));
    }

    if (form.password.length < 6) {
      return (Swal.fire({
        icon: 'error',
        title: 'Hey, maidenless...!',
        text: 'Password must be more than 6 symbols!',
        color: 'white',
        background: 'black',
      }));
    }

    getUserByUsername(form.username)
    .then((snap) => {
      if (snap.exists()) {
        return (Swal.fire({
          icon: 'error',
          title: 'Hey, maidenless...!',
          text: `Username ${form.username} is already taken.`,
          color: 'white',
          background: 'black',
        }));
      }

      return registerUser(form.email, form.password)
      .then((u) => {
        createUserUsername(form.username, u.user.uid, u.user.email, form.firstName, form.lastName)
        .then(() => {
          return (Swal.fire({
            icon: 'success',
            title: 'Hey there tarnished!',
            text: 'You are no longer maidenless.',
            color: 'white',
            background: 'black',
          }));
        });
        navigate('/profile');
      });
    }).catch((e) => {
      if (e) {
        Swal.fire({
          icon: 'error',
          title: 'Hey, maidenless...!',
          text: 'Invalid e-mail entered!',
          color: 'white',
          background: 'black',
        });
      }
    });
  }

  return (
    <Main title='Register'>
      <div className='Register'>
        <input className="registration-input" autoComplete="off" placeholder="E-Mail" type="email" id="email" onChange={update('email')} value={form.email}></input>
        <input className="registration-input" autoComplete="off" placeholder="Потребителско име" type="text" id="username" onChange={update('username')} value={form.username}></input>
        <input className="registration-input" autoComplete="off" placeholder="Парола" type="password" id="password" onChange={update('password')} value={form.password}></input>
        <input className="registration-input" autoComplete="off" placeholder="Име" type="text" id="firstName" value={form.firstName} onChange={update('firstName')}></input>
        <input className="registration-input" autoComplete="off" placeholder="Фамилия" type="text" id="lastName" value={form.lastName} onChange={update('lastName')}></input>
        <select name="tarnished" className="registration-input">
          <option value={'Tarnished'}>Tarnished</option>
          <option disabled>Male</option>
          <option disabled>Female</option>
          <option disabled>Chad</option>
          <option disabled>Stacy</option>
        </select>
        <button className="register-button" onClick={register}>Регистрирай се</button>
      </div>
    </Main>
  );
};

export default Register;