import Main from '../Main/Main';
import loadingGear from '../../assets/loading-gear.gif';
import './Loading.css';

const Loading = () => {

  return (
      <img className='loading' src={loadingGear} alt='loading'/>
  )
}

export default Loading;