import React from 'react';
import Main from '../Main/Main';
import './Videos.css';

export const Videos = () => {
  return (
    <Main title='Videos'>
      <div className='all-videos-container'>
      <div className='video-container'>
        <iframe width="490" height="305" id="vid" src="https://www.youtube.com/embed/E3Huy2cdih0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div className='video-container'>
        <iframe width="490" height="305" id="vid" src="https://www.youtube.com/embed/K_03kFqWfqs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div className='video-container'>
        <iframe width="490" height="305" id="vid" src="https://www.youtube.com/embed/AKXiKBnzpBQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div className='video-container'>
        <iframe width="490" height="305" id="vid" src="https://www.youtube.com/embed/qqiC88f9ogU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      </div>
    </Main>
  );
};
