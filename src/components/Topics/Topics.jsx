import './Topics.css'
import { useNavigate ,useParams ,Link} from 'react-router-dom'
import { useContext, useEffect, useState } from 'react'
import {getAllTopicsForCategory, createTopicForCategory} from '../../services/category.service'
import ListLink from '../ListLink/ListLink'
import { AppContext } from '../../providers/AppContext'
import TopicBox from '../TopicBox/TopicBox'
import Main from '../Main/Main'
const Topics = () => {
  const [form, setForm] = useState({ category:'', title: '',author:'',createdOn:'',posts:['']})
  const [topics,setTopics] = useState([])
  const navigate = useNavigate()
  const {id} = useParams()
  const { userData , setUser} = useContext(AppContext)
  const update = (prop) => (event) => {
    setForm({
      ...form,
      [prop]: event.target.value
    })
  }
  const updateData = async (id) => {
    const data = await getAllTopicsForCategory(id)
    setTopics(data)
  }

  useEffect(() => {
    updateData(id)
  },[])
  const createTopic = async (e) => {
    e.preventDefault()
    if(!form.title) {
      return alert('Please provide a title for the topic')
    }
    await createTopicForCategory(id,form.title, new Date(),userData.username)
    updateData(id)
  }
  return(
    <Main title={id}>
    <div className="container">
      <div className='topics-header'>
        <span className='category-topic-title' id="topic-title-active"><Link to={`/forum`}>Категории</Link></span>
        <span className='category-topic-title' id="topic-title">{id}</span>
        <span className='category-topic-title' id="topic-statistic">Статистика</span>
       </div>
      <div className='topics-container'>
       {topics ? topics.map((topic,key) =>(
        <div>
          <TopicBox topic={topic} updateData={updateData} role={userData?.role}/>
        </div>
       )) : <div>
              <h1>Съзадай първата тема за тази Категория</h1>
          </div>}
      </div>
      <div>
      {userData?.role >= 1 ? <div className='topic-container'>
        <form onSubmit={createTopic}>
          <div id="topic-input">
          <label htmlFor='topic-title' className="labels"></label>
          <input type="text" id="topic-title-create" onChange={update('title')} value={form.title} className="registration-input" placeholder='Заглавие на Темата'></input>
          </div>
          <button id="topic-create"type="submit" onSubmit={createTopic} className="register-button">Създай тема</button>
      </form>
      </div> : <></>}
      </div>
    </div>
    </Main>
  )
}
export default Topics