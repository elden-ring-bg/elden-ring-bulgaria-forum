
import { Link,useNavigate ,NavLink} from 'react-router-dom'
import './BoxLink.css'
import skull from '../../assets/skull.png'
const BoxLink = (data) => {
  const navigate = useNavigate()
  return (<div>
    <Link to={'/'+data.route}>
      <div className='box-container'>
        <img className='box-img' src={data.source}></img>
        <p>{data.text}</p>
      </div>
    </Link>
  </div>
  )
}
export default BoxLink