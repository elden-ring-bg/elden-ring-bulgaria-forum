import './TopicBox.css'
import { Link, useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { deleteEverythingTopic } from '../../services/category.service'
const TopicBox = ({topic,updateData,role}) => {
  const navigate = useNavigate()
  const removeTopic = async () => {
    await deleteEverythingTopic(topic.id,topic.category)
    await updateData(topic.category)
  }
  if(!topic) {
    console.log('no topic')
  }
  //https://firebasestorage.googleapis.com/v0/b/elden-ring-bulgaria.appspot.com/o/category-defaults%2FWretch.png?alt=media&token=abf8d1a8-0fc0-4ef7-a879-0b3ce825336e
  return (
    <div className='topic-box'>
      <img id="topic-img"></img>
      <div className='topic-box-text'>
        <Link to={'/posts'+ topic.id}><h1>{topic.title}</h1></Link>
        <p id="topic-info">Автор: <span id="topic-author">{topic.author}</span> на <span>{new Date(topic.createdOn).toLocaleDateString()}</span> - <span>{new Date(topic.createdOn).toLocaleTimeString('it-IT')}</span></p>
        {role >= 2 ? <span className='delete-topic' onClick={removeTopic}>Delete</span> : <></>}
      </div>
      <div id="topic-stats">
        <span id="topic-posts">Постове: {topic.posts[0] === "" ? 0 : topic.posts.length}</span>
      </div>
    </div>
  )
}

export default TopicBox