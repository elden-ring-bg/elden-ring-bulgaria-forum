import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
import { getDatabase } from "firebase/database"
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "AIzaSyC8_YRSdg14cr6KenFDe6jgkWe8yB06bLw",
  authDomain: "elden-ring-bulgaria.firebaseapp.com",
  projectId: "elden-ring-bulgaria",
  storageBucket: "elden-ring-bulgaria.appspot.com",
  messagingSenderId: "186017811557",
  appId: "1:186017811557:web:37179b84743f4650360a8c",
  measurementId: "G-KGXNX5D2PB",
  databaseURL: "https://elden-ring-bulgaria-default-rtdb.europe-west1.firebasedatabase.app/",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
