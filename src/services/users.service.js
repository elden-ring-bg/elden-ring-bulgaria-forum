import { get, set, ref, query, equalTo, orderByChild, update } from 'firebase/database';
import { db } from '../config/firebase-config';
import { roles } from '../providers/roles';
import { titles } from '../providers/titles';

export const getUserByUsername = (username) => {

  return get(ref(db, `users/${username}`));
};

export const getAllUsers = () => {
  return get(ref(db, 'users/'));
};

export const createUserUsername = (username, uid, email, firstName, lastName, profilePicUrl) => {

  return set(ref(db, `users/${username}`), { 
    username,
    uid,
    email,
    createdOn: new Date(),
    likedPosts: [''],
    posts: [''], steamId: '',
    role: roles.user,
    firstName,
    lastName,
    title: titles.novice,
    profilePicUrl: 'https://www.kindpng.com/picc/m/21-214439_free-high-quality-person-icon-default-profile-picture.png'});
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/profilePicUrl`]: url,
  });
};

export const updateProfileSteamId = (username, steamId) => {
  return update(ref(db), {
    [`users/${username}/steamId`]: steamId
  });
};

export const updateProfileTitle = (username, title) => {
  return update(ref(db), {
    [`users/${username}/title`]: title
  });
}