
import { get, set, ref,push,update, remove} from 'firebase/database';
import { deleteObject } from 'firebase/storage';
import { db } from '../config/firebase-config';
import { storage } from '../config/firebase-config';
import { ref as storageRef} from 'firebase/storage';

export const createCategory = (title,description,topics) => {
  const reference = ref(db, 'categories/' + title) 
  console.log(title,description,topics)
  set(reference , {
    title,
    description,
    topics,
    img:'https://firebasestorage.googleapis.com/v0/b/elden-ring-bulgaria.appspot.com/o/category-defaults%2Flogo.jpg?alt=media&token=f1c75a71-cde3-42a5-b2a0-e19e0d5420d1'
  })
}

export const getCategory = async (category) => {
  const snapshot = await get(ref(db,`categories/${category}`))
  const data = snapshot.val()
  return data
}
export const getCategories = async () => {
  const reference = ref(db,'categories')
  const data = await get(reference)
  let categories = []
  data.forEach((cat) => {
    categories.push(cat.val())
  })
  return categories
}
export const createTopic = async(title,createdOn, author = 'Anonym',posts,category) => {
  const reference = ref(db,`topics`)
  let key = await push(reference, {
    title,
    author,
    createdOn,
    posts,
    category
  }).then(snap => {
    return snap.key
  })
  //console.log(key)
  return key
}
export const getTopic = async (topic) => {
  const snapshot = await get(ref(db,`topics/${topic}`))
  const data = snapshot.val()
  return data
}
export const createTopicForCategory = async (category,title,createdOn,author,posts = ['']) => {
  const categoryTopics = (await getCategory(category)).topics
  const topicKey = await createTopic(title,createdOn = createdOn.toString(),author,posts = [''],category)
  //console.log(topicKey)
  if(categoryTopics[0] === '') {
    categoryTopics[0] = topicKey
  } else {
    categoryTopics.push(topicKey)
  }
  const topics = {'topics':[...categoryTopics]}
  const reference = ref(db, `categories/${category}`)
  await update(reference,topics)
}
export const getAllTopicsForCategory = async (category) => {
  const categoryTopics = (await getCategory(category)).topics
  if(categoryTopics[0] === '') {
    return null
  }
  const topics = []
  await categoryTopics.reduce(async(acc,topic) => {
    await acc
    let res = await getTopic(topic)
    if(res){
      res.id = topic
      topics.push(res)
    }
  },Promise.resolve())
  return topics
}
export const createPost = async (title,createdOn,description,author,topic,postImages) => {
  const reference = ref(db,`posts`)
  let post = {
      title,
      createdOn:createdOn.toString(),
      description,
      author,
      likedBy:[''],
      likes:0,
      topic,
      id:null,
      postImages,
  }
  const key = await push(reference, post).then(snap => {
    return snap.key
  })
  const updateRef = ref(db,`posts/${key}`)
  post.id = key
  update(updateRef,post)
  return key

}
export const createPostToTopic = async (topicId,createdOn,description,author,postImages) => {
  const topic = await getTopic(topicId)
  const topicPosts = topic.posts
  const topicTitle = topic.title
  let postNumber = ''
  if (topicPosts[0] === '') {
    postNumber = ''
  } else {
    postNumber = topicPosts.length
  }
  const postKey = await createPost(topicTitle,createdOn = createdOn.toString(),description,author,topicId,postImages)

  if(topicPosts[0] === '') {
    topicPosts[0] = postKey
  } else {
    topicPosts.push(postKey)
  }
  const posts = {'posts':[...topicPosts]}
  const reference = ref(db, `topics/${topicId}`)
  await update(reference,posts)
  const userPosts = (await getUser2(author)).posts
  if (userPosts[0] === '') {
    userPosts[0] = postKey
  } else {
    userPosts.push(postKey)
  }
  const uPosts = {'posts':[...userPosts]}
  const userRef = ref(db,`users/${author}`)
  await update(userRef,uPosts)
  const post = await getPost(postKey)
  return [postKey,post]
}

export const getAllPostsForTopic = async (topic) => {
  const categoryTopics = (await getTopic(topic)).posts
  const posts = []
  if(categoryTopics[0] === '') {
    return ['']
  }
  await categoryTopics.reduce(async(acc,post) => {
    await acc
    let res = await getPost(post)
    if(res) {
      res.id = post
      posts.push(res)
    }
  },Promise.resolve())
  return posts
}
export const getPost = async (post) => {
  return (await get(ref(db,`posts/${post}`))).val()
}


export const deleteCategory = (category) => {
  const reference = ref(db,`categories/${category}`)
  remove(reference)
}
export const deleteTopic = (topic) => {
  const reference = ref(db,`topics/${topic}`)
  remove(reference)
}
export const deletePost = (post) => {
  const reference = ref(db,`posts/${post}`)
  remove(reference)
}
export const deleteEverythingCategory = async (category) => {
  const postIds = []
  const topicIds = []
  topicIds.push(...(await getCategory(category)).topics)
  if(topicIds[0] === '') {
    deleteCategory(category)
    return 
  }
  for (let i = 0 ; i< topicIds.length;i++) {
     postIds.push(...(await getTopic(topicIds[i])).posts)
  }
  postIds.forEach(post => {
    deletePost(post)
  })
  topicIds.forEach(topic => {
    deleteTopic(topic)
  })
  deleteCategory(category)
}


export const deleteEverythingTopic = async (topic,category) => {
  const postIds = []
  let topicIds = [] 
  topicIds.push(...(await getCategory(category)).topics)
  postIds.push(...(await getTopic(topic)).posts)
  postIds.forEach((post) => {
    deletePost(post)
  })
  topicIds = topicIds.filter((topicId) =>  topicId !== topic)
  if(topicIds.length === 0) {
    topicIds = ['']
  }
  const topics = {'topics':[...topicIds]}
  const categoryRef = ref(db,`categories/${category}`)
  update(categoryRef,topics)
  deleteTopic(topic)
}

export const updatePost = async(post,newPost) => {
  const reference = ref(db,`posts/${post}`)
  update(reference,newPost)
}
export const deleteOnlyPostTopic = async (post,topic) => {
  const postIds = (await getTopic(topic)).posts
  const usersLiked = post.likedBy
  usersLiked.forEach((user) => {
    addLikedPosts(user,post.id)
  })
  if(post.postImages) {
    post.postImages.forEach((img) => {
      deletePostImage(img)
    })
  }
  let newPostIds = postIds.filter((el) => el !== post.id)
  if(newPostIds.length === 0) {
    newPostIds = ['']
  }
  newPostIds = {'posts':[...newPostIds]}
  const topicRef = ref(db,`topics/${topic}`)
  update(topicRef,newPostIds)
  removeFromUserPosts(post.author,post.id)
  deletePost(post.id)
}

export const getUser2 = async (username) => {
  return (await get(ref(db,`users/${username}`))).val()
} 

export const addLikedPosts = async (username,postId) => {
  let userLikedPosts = (await getUser2(username)).likedPosts
  if(!userLikedPosts) {
    userLikedPosts = ['']
  }
  if(userLikedPosts[0] === '') {
    userLikedPosts[0] = postId
  } else {
    if(userLikedPosts.includes(postId)) {
      userLikedPosts = userLikedPosts.filter((el) => el !== postId)
    }else {
      userLikedPosts.push(postId)
    }
  }
  userLikedPosts = {'likedPosts':[...userLikedPosts]}
  const userRef = ref(db, `users/${username}`)
  update(userRef,userLikedPosts)
}

export const removeFromUserPosts = async (username,postId) => {
  const userPosts = (await getUser2(username)).posts
  let newUserPosts = userPosts.filter((post) => post !== postId)
  if(newUserPosts.length === 0) {
    newUserPosts = [""]
  }
  newUserPosts = {'posts':[...newUserPosts]}
  const userRef = ref(db,`users/${username}`)
  update(userRef,newUserPosts)
}
export const getAllAuthors = async (posts) => {
  let userData = []
  await posts.reduce(async(acc,postId) => {
    await acc
    let res = await getUser2(postId.author)
    userData.push(res)
  },Promise.resolve())
  return userData
}

const deletePostImage = (imgId) => {
  const reference = storageRef(storage, `/postImages/${imgId}`);
  deleteObject(reference)
}


export const getAllTopicsPosts = async() => {
  const reference = ref(db,`topics`)
  const data = await (await get(reference)).val()
  let mostActiveTopics = []
  let maxes = []
  Object.keys(data).forEach((topic) => {
    //console.log('data',data[topic].posts)
    let posts = data[topic].posts.length
    maxes.push(posts)
  })
  if(!maxes) {
    return ""
  }
  maxes.sort((a,b) => b-a) 
  maxes.splice(3)
  Object.keys(data).forEach((topic) => {
    let posts = data[topic].posts.length
      if(maxes.includes(posts)) {
        data[topic].id = topic
        mostActiveTopics.push(data[topic])
        maxes.splice(maxes.indexOf(posts),1)
    }
  })

  return mostActiveTopics
}