export const titles = {
  novice: 'Tarnished',
  apprentice: 'Traveling Merchant',
  intermediate: 'Pot Warrior',
  advanced: 'Roundtable Hold Knight',
  expert: 'Finger Reader',
  master: 'Elden God\'s Consort',
  god: 'Elden Lord',
};