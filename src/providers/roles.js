export const roles = {
  user: 1,
  moderator: 2,
  admin: 3,
  blocked: 4,
};