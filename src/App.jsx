import Header from './components/Header/Header';
import './App.css';
import Footer from './components/Footer/Footer';
import Home from './components/Home/Home'
import { AppContext } from './providers/AppContext';
import { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import Register from './components/Register/Register';
import { Forum } from './components/Forum/Forum';
import LeftPanel from './components/LeftPanel/LeftPanel'
import RightPanel from './components/RightPanel/RightPanel';
import { Login } from './components/Login/Login';
import { auth } from './config/firebase-config';
import { useAuthState } from 'react-firebase-hooks/auth'
import { getUserData } from './services/users.service';
import Category from './components/Categories/Category';
import Topics from './components/Topics/Topics';
import Posts from './components/Posts/Posts';
import Main from './components/Main/Main';
import Editor from './components/Editor/Editor';
import Profile from './components/Profile/Profile';
import { GameInfo } from './components/GameInfo/GameInfo';
import { Map } from './components/Map/Map';
import { Media } from './components/Media/Media';
import { Videos } from './components/Videos/Videos';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
import Streams from './components/Streems/Streams';
import About from './components/About/About';
const App = () => {

  const [appState, setAppState] = useState({ user: null, userData: null });
  const [user] = useAuthState(auth);

  useEffect(() => {
    if (user === null) {
      return;
    }
    getUserData(user.uid)
    .then((snap) => {
      if (!snap.exists()) {
        alert('App getUserData alert');
      }

      setAppState({ user, userData: snap.val()[Object.keys(snap.val())[0]]});
    }).catch((e) => {
      alert(e.message);
    })
  }, [user]);
  
  return (
    <AppContext.Provider value={{...appState, setContext: setAppState}}>
      <div className='App'>
        <Header />
        <LeftPanel/>
        <ScrollToTop/>
        <Routes>
          <Route path='/' element={<Home/>}></Route>
          <Route path='/home' element={<Home/>}></Route>
          <Route path='/register' element={<Register/>}></Route>
          <Route path='/login' element={<Login/>}></Route>
          <Route path='/logout' element={<Home/>}></Route>
          <Route path='/profile' element={<Profile/>}></Route>
          <Route path='/gameinfo' element={<GameInfo/>}></Route>
          <Route path='/map' element={<Map/>}></Route>
          <Route path='/media' element={<Media/>}></Route>
          <Route path='/videos' element={<Videos/>}></Route>
          <Route path='/gallery' element={<Media/>}></Route>
          <Route path='/streams' element={<Streams/>}></Route>
          <Route path='/about' element={<About/>}></Route>
          {/* <Route path='/Forum' element={<Forum/>}></Route> */}
          <Route path='/forum' element={<Category/>}></Route>
          <Route path='/category/:id' element={<Topics/>}></Route>
          <Route path='/posts:id' element={<Posts/>}></Route>
        </Routes>
        <RightPanel/>
      </div>
      <Footer />
    </AppContext.Provider>
  );
}

export default App;
