# Getting Started with this app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Starting the program

You can't start the program without the proper node modules so first you need to run

### `npm install`

Then after the modules are installed run

### `npm start`

This runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

When you first start the app you should be redirected to the home page which will look something like this.

![home-page](https://i.imgur.com/M7N5Yrh.png)

From that point you can explore the pages as you like.

You can register and try to write something in the forum.

Also be sure to check out the interactive map.